package ru.example.mygallery;

/**
 * Работа с элементом галереи
 *
 * @author Ivanchin E.I. Brodskiy L.E.
 * @version 1.0
 */
class Cell {
    private String title;
    private String path;

    /**
     * получение Имени файла
     * @return Имя файла
     */
    String getTitle() {
        return title;
    }
    /**
     * задание Имени файла
     * @return Имя файла
     */
    void setTitle(String title) {
        this.title = title;
    }
    /**
     * получение пути до файла
     * @return Имя файла
     */
    String getPath() {
        return path;
    }
    /**
     * задание пути до файла
     * @return путь до файла
     */
    void setPath(String path) {
        this.path = path;
    }
}
