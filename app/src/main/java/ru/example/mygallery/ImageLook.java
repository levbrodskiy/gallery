package ru.example.mygallery;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.widget.ImageView;

/**
 * Просмотр изображения при клике
 *
 * @author Ivanchin E.I. Brodskiy L.E.
 * @version 1.0
 */
public class ImageLook extends Activity {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.image_open);

        Intent intent = getIntent();
        String path = intent.getStringExtra("Path");
        ImageView imageView =  findViewById(R.id.image);

        imageView.setImageDrawable(Drawable.createFromPath(path));

    }

}
